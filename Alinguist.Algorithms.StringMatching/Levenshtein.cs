﻿using System;

namespace Alinguist.Algorithms.StringMatching
{
    public class Levenshtein : IStringMatcher
    {
        public int? Compute(string left, string right)
        {
            if(left == null
               || right == null)
                return null;

            var n = left.Length;
            var m = right.Length;
            var d = new int[n + 1, m + 1];

            // Step 1
            if(n == 0)
                return m;

            if(m == 0)
                return n;

            // Step 2
            for(var i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for(var j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for(var i = 1; i <= n; i++)
            {
                //Step 4
                for(var j = 1; j <= m; j++)
                {
                    // Step 5
                    var cost = (right[j - 1] == left[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }

            // Step 7
            return d[n, m];
        }
    }
}
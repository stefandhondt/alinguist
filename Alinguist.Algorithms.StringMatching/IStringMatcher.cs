﻿namespace Alinguist.Algorithms.StringMatching
{
    public interface IStringMatcher
    {
        int? Compute(string left, string right);
    }
}
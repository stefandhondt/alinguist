﻿namespace Alinguist.IO
{
    public interface IResourceReader<T>
    {
        ReadResult<T> Read();
    }
}
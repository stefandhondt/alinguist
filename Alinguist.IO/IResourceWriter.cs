﻿using System.Collections.Generic;
using Alinguist.IO.Model;

namespace Alinguist.IO
{
    public interface IResourceWriter<T>
    {
        WriteResult<T> Write(T data);
    }
}
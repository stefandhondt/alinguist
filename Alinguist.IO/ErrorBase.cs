﻿using System;

namespace Alinguist.IO
{
    public abstract class ErrorBase
    {
        private readonly int errorCode;
        private readonly string reason;
        private readonly Exception exception;

        public virtual int ErrorCode => errorCode;
        public virtual string Reason => reason;
        public virtual Exception Exception => exception;

        public ErrorBase(int errorCode, string reason, Exception exception = null)
        {
            this.errorCode = errorCode;
            this.reason = reason;
            this.exception = exception;
        }
    }
}
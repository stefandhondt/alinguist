﻿using System.Collections.Generic;
using System.Linq;

namespace Alinguist.IO
{
    public class ReadResult<T>
    {
        private bool success;
        private T result;
        private List<IoError> errors;

        public virtual bool Success => success;
        public virtual T Result => result;
        public virtual List<IoError> Errors => errors;

        protected ReadResult()
        {
        }

        public static ReadResult<T> Create(bool success, T result, IEnumerable<IoError> errors = null)
        {
            return new ReadResult<T>
            {
                success = success,
                result = result,
                errors = errors != null ? errors.ToList() : new List<IoError>()
            };
        }
    }
}
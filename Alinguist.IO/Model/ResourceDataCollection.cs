﻿using System.Collections.Generic;
using System.Globalization;
using Alinguist.Exceptions.Culture;
using Alinguist.Exceptions.ResourceCollection;
using ALinguist.Shared.Extensions;

namespace Alinguist.IO.Model
{
    public class ResourceDataCollection
    {
        private string name;
        private CultureInfo culture;
        private List<ResourceData> resources;

        public virtual string Name => name;
        public virtual CultureInfo Culture => culture;
        public virtual List<ResourceData> Resources => resources;

        protected ResourceDataCollection()
        {
            resources = new List<ResourceData>();
        }

        public static ResourceDataCollection Create(string name, string culture, IEnumerable<ResourceData> resources = null)
        {
            var collection = new ResourceDataCollection()
                .SetName(name)
                .SetCulture(culture);

            if(resources != null)
            {
                foreach(var resource in resources)
                    collection.AddResource(resource);
            }

            return collection;
        }

        public virtual ResourceDataCollection SetName(string name)
        {
            if(string.IsNullOrWhiteSpace(name))
                throw new NameEmptyException();

            this.name = name;

            return this;
        }

        public virtual ResourceDataCollection SetCulture(string culture)
        {
            if(string.IsNullOrWhiteSpace(culture))
                throw new CultureEmptyException();

            this.culture = culture.AsCulture();

            if(culture == null)
                throw new InvalidCultureException(culture);

            return this;
        }

        public virtual ResourceDataCollection SetCulture(CultureInfo culture)
        {
            this.culture = culture ?? throw new InvalidCultureException();

            return this;
        }

        public virtual ResourceDataCollection AddResource(ResourceData resource)
        {
            if(resource == null)
                throw new Exceptions.Resource.InvalidResourceException();

            resources.Add(resource);

            return this;
        }
    }
}
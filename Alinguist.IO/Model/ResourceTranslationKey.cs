﻿namespace Alinguist.IO.Model
{
    public class ResourceTranslationKey
    {
        public string Culture { get; set; }
        public int TranslationResourceId { get; set; }
    }
}
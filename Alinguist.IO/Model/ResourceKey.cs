﻿namespace Alinguist.IO.Model
{
    public class ResourceKey
    {
        public int Id { get; set; }

        public ResourceKey(int id)
        {
            Id = id;
        }
    }
}
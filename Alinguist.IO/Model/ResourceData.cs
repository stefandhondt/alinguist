﻿using Alinguist.Exceptions.Culture;
using Alinguist.Exceptions.Resource;

namespace Alinguist.IO.Model
{
    public class ResourceData
    {
        private ResourceKey key;
        private string group;
        private string name;
        private string value;
        private string culture;

        public ResourceKey Key => key;
        public virtual string Group => group;
        public virtual string Name => name;
        public virtual string Value => value;
        public virtual string Culture => culture;

        protected ResourceData()
        {
        }

        public static ResourceData Create(ResourceKey key, string culture, string group, string name, string value)
        {
            return new ResourceData()
                .SetKey(key)
                .SetCulture(culture)
                .SetGroup(group)
                .SetName(name)
                .SetValue(value);
        }

        public static ResourceData Create(string culture, string group, string name, string value)
        {
            return ResourceData.Create(null, culture, @group, name, value);
        }

        public static ResourceData Create(ResourceData resource)
        {
            return ResourceData.Create(resource?.Key, resource?.Culture, resource?.Group, resource?.Name, resource?.Value);
        }

        public virtual ResourceData SetKey(ResourceKey key)
        {
            this.key = key;

            return this;
        }

        public virtual ResourceData SetCulture(string culture)
        {
            if(string.IsNullOrWhiteSpace(culture))
                throw new CultureEmptyException();

            this.culture = culture;

            return this;
        }

        public virtual ResourceData SetGroup(string group)
        {
            if(string.IsNullOrWhiteSpace(group))
                throw new NameEmptyException();

            this.group = group;

            return this;
        }

        public virtual ResourceData SetName(string name)
        {
            if(string.IsNullOrWhiteSpace(name))
                throw new NameEmptyException();

            this.name = name;

            return this;
        }

        public virtual ResourceData SetValue(string value)
        {
            this.value = value;

            return this;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alinguist.IO.Validation
{
    public static class ValidationCodes
    {
        public static class Regex
        {
            public static int NoMatchFound = 1001;
        }

        public static class Xsd
        {
            public static int NoXmlSchemaProvided = 1011;
            public static int XmlSchemaValidationFailed = 1012;
            public static int NoXmlProvided = 1013;
        }

        public static class ResourcesFilename
        {
            public static int NoFilenameProvided = 1021;
            public static int FilenameWithoutExtension = 1022;
            public static int InvalidFilenameExtension = 1023;
            public static int InvalidCulture = 1024;
            public static int InvalidName = 1025;
        }
    }
}

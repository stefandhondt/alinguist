﻿using System.Collections.Generic;
using System.Linq;

namespace Alinguist.IO.Validation
{
    public class ValidationResult
    {
        private bool valid;
        private List<ValidationError> errors;

        public virtual bool Valid => valid;
        public virtual List<ValidationError> Errors => errors;

        protected ValidationResult()
        {
        }

        protected ValidationResult(IEnumerable<ValidationError> errors = null)
        {
            this.errors = errors?.ToList() ?? new List<ValidationError>();
            valid = !errors?.Any() ?? true;
        }

        public static ValidationResult Create(IEnumerable<ValidationError> errors = null)
        {
            return new ValidationResult((errors?.Any() ?? false) ? errors : null);
        }
    }
}
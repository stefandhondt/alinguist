﻿using System;

namespace Alinguist.IO.Validation
{
    public class ValidationError : ErrorBase
    {
        public ValidationError(int errorCode, string reason, Exception exception = null)
            : base(errorCode, reason, exception)
        {
        }
    }
}
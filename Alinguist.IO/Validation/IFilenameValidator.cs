﻿namespace Alinguist.IO.Validation
{
    public interface IFilenameValidator : IValidator
    {
        string Filename { get; set; }
    }
}
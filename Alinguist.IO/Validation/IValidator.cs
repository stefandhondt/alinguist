﻿namespace Alinguist.IO.Validation
{
    public interface IValidator
    {
        ValidationResult Validate();
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Alinguist.IO.Extensions;
using ALinguist.Shared.Extensions;
using Codes = Alinguist.IO.Validation.ValidationCodes.ResourcesFilename;

namespace Alinguist.IO.Validation
{
    public class ResourcesFilenameValidator : IFilenameValidator
    {
        private const string extension = "resources";

        public string Filename { get; set; }

        public virtual ValidationResult Validate()
        {
            var errors = new List<ValidationError>();

            if(string.IsNullOrWhiteSpace(Filename))
                errors.Add(new ValidationError(Codes.NoFilenameProvided, "Filename empty"));
            else
            {
                var ext = Path.GetExtension(Filename);

                if(string.IsNullOrWhiteSpace(ext))
                    errors.Add(new ValidationError(Codes.FilenameWithoutExtension, "No extension"));
                else
                {
                    if(ext.ToLower() != extension)
                        errors.Add(new ValidationError(Codes.InvalidFilenameExtension, $"Invalid extension. Was '{ext}' but should be '{extension}'."));
                }

                var nameWithoutExt = Path.GetFileNameWithoutExtension(Filename);
                var separator = "_";
                var tokens = nameWithoutExt.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries)?.ToList();
                var culture = tokens.Last().AsCulture();
                tokens.RemoveAt(tokens.Count - 1);

                if(culture == null)
                    errors.Add(new ValidationError(Codes.InvalidCulture, $"Invalid culture '{culture}'."));

                if(!tokens.Any())
                    errors.Add(new ValidationError(Codes.InvalidName, "Filename only consisted of culture."));
            }

            return errors.AsValidationResult();
        }
    }
}
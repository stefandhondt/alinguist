﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Schema;
using Alinguist.IO.Extensions;
using Codes = Alinguist.IO.Validation.ValidationCodes.Xsd;

namespace Alinguist.IO.Validation
{
    public class XsdValidator : IValidator
    {
        public virtual XDocument Xml { get; set; }
        public virtual XmlSchemaSet XmlSchemas { get; set; }

        public virtual ValidationResult Validate()
        {
            var errors = new List<ValidationError>();

            if(Xml == null)
                errors.Add(new ValidationError(Codes.NoXmlProvided, "No XML provided."));

            if(XmlSchemas == null
                || XmlSchemas.Count == 0)
                errors.Add(new ValidationError(Codes.NoXmlSchemaProvided, "No XSD-schemas provided."));
            else
            {
                Xml?.Validate(XmlSchemas, (o, e) =>
                {
                    errors.Add(new ValidationError(Codes.XmlSchemaValidationFailed, e.Message, e.Exception));
                });
            }

            return errors.AsValidationResult();
        }
    }
}

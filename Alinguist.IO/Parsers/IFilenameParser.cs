﻿namespace Alinguist.IO.Parsers
{
    public interface IFilenameParser<T>
    {
        string Filename { get; }
        T Parse();
    }
}
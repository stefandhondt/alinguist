﻿using System.Collections.Generic;
using Alinguist.IO.Validation;

namespace Alinguist.IO.Parsers
{
    public class ResourcesFilenameParserResult : ValidationResult
    {
        private string name;
        private string culture;
        private IEnumerable<ValidationError> errors;

        public virtual string Name => name;
        public virtual string Culture => culture;

        protected ResourcesFilenameParserResult()
        {
        }

        protected ResourcesFilenameParserResult(string name, string culture, IEnumerable<ValidationError> errors)
            : base(errors)
        {
            this.name = name;
            this.culture = culture;
        }

        public static ResourcesFilenameParserResult Create(string name, string culture, IEnumerable<ValidationError> errors = null)
        {
            return new ResourcesFilenameParserResult(name, culture, errors);
        }
    }
}
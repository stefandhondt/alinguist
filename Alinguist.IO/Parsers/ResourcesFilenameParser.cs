﻿using System;
using System.IO;
using System.Linq;
using Codes = Alinguist.IO.Validation.ValidationCodes.ResourcesFilename;

namespace Alinguist.IO.Parsers
{
    public class ResourcesFilenameParser : IFilenameParser<ResourcesFilenameParserResult>
    {
        private string filename;

        public virtual string Filename => filename;

        protected ResourcesFilenameParser()
        {
        }

        public static ResourcesFilenameParser Create(string filename)
        {
            return new ResourcesFilenameParser
            {
                filename = filename
            };
        }

        public virtual ResourcesFilenameParserResult Parse()
        {
            if(string.IsNullOrWhiteSpace(Filename))
                return ResourcesFilenameParserResult.Create(null, null, new[] { new Validation.ValidationError(Codes.NoFilenameProvided, "Filename empty.") });

            var nameWithoutExt = Path.GetFileNameWithoutExtension(Filename);
            var separator = "_";
            var tokens = nameWithoutExt.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries)?.ToList();
            
            if(tokens.Count < 2)
                return ResourcesFilenameParserResult.Create(null, null, new[] { new Validation.ValidationError(Codes.InvalidName, "Filename invalid.") });

            var culture = tokens.Last();
            tokens.RemoveAt(tokens.Count - 1);
            var name = string.Join(separator, tokens);

            return ResourcesFilenameParserResult.Create(name, culture, null);
        }
    }
}
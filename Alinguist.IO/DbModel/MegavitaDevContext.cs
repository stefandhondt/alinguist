using System.Data.Entity;

namespace Alinguist.IO.DbModel
{
    public partial class MegaVitaDevContext : DbContext
    {
        public MegaVitaDevContext()
            : base("name=MegaVitaDevContext")
        {
        }

        public virtual DbSet<Translation> Translations { get; set; }
        public virtual DbSet<TranslationResource> TranslationResources { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

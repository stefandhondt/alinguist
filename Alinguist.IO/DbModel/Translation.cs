using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Alinguist.IO.DbModel
{
    [Table("Resource.Translation")]
    public partial class Translation
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string Culture { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TranslationResourceId { get; set; }

        [Required]
        public string Value { get; set; }

        public virtual TranslationResource TranslationResource { get; set; }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Alinguist.IO.DbModel
{
    [Table("Resource.TranslationResource")]
    public partial class TranslationResource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TranslationResource()
        {
            Translations = new HashSet<Translation>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string Group { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Translation> Translations { get; set; }
    }
}

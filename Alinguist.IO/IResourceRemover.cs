﻿using System.Collections.Generic;
using Alinguist.IO.Model;

namespace Alinguist.IO
{
    public interface IResourceRemover
    {
        void RemoveTranslationResource(ResourceKey data);
        void RemoveTranslationResource(IEnumerable<ResourceKey> data);
    }

    public interface IResourceTranslationRemover
    {
        void RemoveTranslation(ResourceTranslationKey data);
        void RemoveTranslation(IEnumerable<ResourceTranslationKey> data);
    }
}
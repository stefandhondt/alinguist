﻿using System;

namespace Alinguist.IO
{
    public class IoError : ErrorBase
    {
        public IoError(int errorCode, string reason, Exception exception = null)
            : base(errorCode, reason, exception)
        {
        }
    }
}
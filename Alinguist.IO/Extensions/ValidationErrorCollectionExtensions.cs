﻿using System.Collections.Generic;
using System.Linq;
using Alinguist.IO.Validation;

namespace Alinguist.IO.Extensions
{
    public static class ValidationErrorCollectionExtensions
    {
        public static ValidationResult AsValidationResult(this IEnumerable<ValidationError> errors)
        {
            return (errors.Any())
                ? ValidationResult.Create(errors)
                : ValidationResult.Create();
        }
    }
}

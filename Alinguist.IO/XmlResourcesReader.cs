﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Alinguist.IO.Model;
using Alinguist.IO.Parsers;
using Alinguist.IO.Validation;

namespace Alinguist.IO
{
    /// <summary>
    /// Reads a legacy .resources file and present it as a <see cref="ResourceDataCollection"/> 
    /// </summary>
    public class XmlResourcesReader : IResourceReader<ResourceDataCollection>
    {
        private string filename;
        private XDocument xml;
        private List<IValidator> validators;
        private IFilenameParser<ResourcesFilenameParserResult> filenameParser;
        private ResourceDataCollection dataCollection;

        public virtual string Filename => filename;
        public virtual List<IValidator> Validators => validators;
        public virtual IFilenameParser<ResourcesFilenameParserResult> FilenameParser => filenameParser;

        protected XmlResourcesReader()
        {
        }

        public static XmlResourcesReader Create(string filename, IFilenameParser<ResourcesFilenameParserResult> filenameParser, IEnumerable<IValidator> validators = null)
        {
            if(filenameParser == null)
                throw new ArgumentNullException(nameof(filenameParser));

            return new XmlResourcesReader()
            {
                filename = filename,
                filenameParser = filenameParser,
                validators = validators?.Where(v => v != null)?.ToList() ?? new List<IValidator>()
            };
        }

        public virtual ReadResult<ResourceDataCollection> Read()
        {
            var errors = Validate() ?? new List<IoError>();

            if(errors?.Any() ?? false)
                return ReadResult<ResourceDataCollection>.Create(false, null, errors);

            try
            {
                var filenameParserResult = FilenameParser.Parse();
                dataCollection = ResourceDataCollection.Create(filenameParserResult.Name, filenameParserResult.Culture);
                xml = XDocument.Load(Filename);
                var translations = Parse(xml);
                dataCollection.Resources.AddRange(translations);
            }
            catch(Exception ex)
            {
                errors.Add(new IoError(0, ex.Message, ex));
            }

            return (errors.Any())
                ? ReadResult<ResourceDataCollection>.Create(false, null, errors)
                : ReadResult<ResourceDataCollection>.Create(true, dataCollection);
        }

        internal virtual List<IoError> Validate()
        {
            var results = new List<IoError>();

            foreach(var validator in Validators)
            {
                var result = validator.Validate();

                if(!result.Valid)
                {
                    if(result.Errors?.Any() ?? false)
                        results.AddRange(result.Errors.Select(e => new IoError(e.ErrorCode, e.Reason, e.Exception)));
                }
            }

            return results;
        }

        internal virtual List<ResourceData> Parse(XDocument xml)
        {
            throw new NotImplementedException();
            //var result = new List<ResourceData>();
            //var resourceElements = xml.Descendants($"{{{xml.Root.GetDefaultNamespace()}}}ResourceData");

            //foreach(var resourceElement in resourceElements)
            //{
            //    var id = resourceElement.Element($"{{{xml.Root.GetDefaultNamespace()}}}ResourceId").Value;
            //    var translation = resourceElement.Element($"{{{xml.Root.GetDefaultNamespace()}}}StringValue").Value;
            //    result.Add(ResourceData.Create(id, translation));
            //}

            //return result;
        }
    }
}
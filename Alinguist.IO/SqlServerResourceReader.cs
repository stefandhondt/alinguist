﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Alinguist.IO.DbModel;
using Alinguist.IO.Model;

namespace Alinguist.IO
{
    public class SqlServerResourceReader : IResourceReader<List<ResourceData>>, IResourceWriter<ResourceData>, IResourceWriter<List<ResourceData>>, IResourceRemover, IResourceTranslationRemover
    {
        protected SqlServerResourceReader()
        {
        }

        public static SqlServerResourceReader Create()
        {
            return new SqlServerResourceReader();
        }

        public ReadResult<List<ResourceData>> Read()
        {
            var errors = new List<IoError>();
            var dataCollections = new List<ResourceData>();

            using(var context = new MegaVitaDevContext())
            {
                var translations = context.Translations.Include(t => t.TranslationResource).ToList();
                dataCollections.AddRange(translations.Select(t => ResourceData.Create(new ResourceKey(t.TranslationResourceId),  t.Culture, t.TranslationResource.Group, t.TranslationResource.Name, t.Value)));
            }

            return (errors.Any())
                ? ReadResult<List<ResourceData>>.Create(false, null, errors)
                : ReadResult<List<ResourceData>>.Create(true, dataCollections);
        }

        public WriteResult<ResourceData> Write(ResourceData resource)
        {
            var errors = new List<IoError>();
            var result = ResourceData.Create(resource);

            try
            {
                using(var context = new MegaVitaDevContext())
                {
                    var translationResource = context.TranslationResources
                        .Include(tr => tr.Translations)
                        .FirstOrDefault(tr => (resource.Key != null && tr.Id == resource.Key.Id)
                                              || (tr.Group == resource.Group && tr.Name == resource.Name));

                    //Check if translation resource already exists.
                    if(translationResource == null)
                        translationResource = context.TranslationResources.Add(new TranslationResource
                        {
                            Group = resource.Group,
                            Name = resource.Name
                        });
                    else
                    {
                        translationResource.Group = resource.Group;
                        translationResource.Name = resource.Name;
                    }

                    //Check if translation already exists.
                    var translation = translationResource.Translations.SingleOrDefault(t => t.Culture == resource.Culture);

                    if(translation == null)
                    {
                        translationResource.Translations.Add(new Translation
                        {
                            Culture = resource.Culture,
                            Value = resource.Value
                        });
                    }
                    else
                    {
                        translation.Value = resource.Value;
                    }

                    context.SaveChanges();
                    result.SetKey(new ResourceKey(translationResource.Id));
                }
            }
            catch(Exception ex)
            {
                errors.Add(new IoError(0, ex.Message, ex));
            }

            return (errors.Any())
                ? WriteResult<ResourceData>.Create(false, null, errors)
                : WriteResult<ResourceData>.Create(true, result);
        }

        public WriteResult<List<ResourceData>> Write(List<ResourceData> resources)
        {
            throw new NotImplementedException();
        }

        public void RemoveTranslationResource(ResourceKey data)
        {
            throw new NotImplementedException();
        }

        public void RemoveTranslationResource(IEnumerable<ResourceKey> data)
        {
            throw new NotImplementedException();
        }

        public void RemoveTranslation(ResourceTranslationKey data)
        {
            throw new NotImplementedException();
        }

        public void RemoveTranslation(IEnumerable<ResourceTranslationKey> data)
        {
            throw new NotImplementedException();
        }
    }
}
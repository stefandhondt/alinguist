﻿using System.Collections.Generic;
using System.Linq;

namespace Alinguist.IO
{
    public class WriteResult<T>
    {
        private bool success;
        private T result;
        private List<IoError> errors;

        public virtual bool Success => success;
        public virtual T Result => result;
        public virtual List<IoError> Errors => errors;

        protected WriteResult()
        {
        }

        public static WriteResult<T> Create(bool success, T result, IEnumerable<IoError> errors = null)
        {
            return new WriteResult<T>
            {
                success = success,
                result = result,
                errors = errors != null ? errors.ToList() : new List<IoError>()
            };
        }
    }
}
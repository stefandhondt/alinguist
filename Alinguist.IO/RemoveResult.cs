﻿using System.Collections.Generic;
using System.Linq;

namespace Alinguist.IO
{
    public class RemoveResult<T>
    {
        private bool success;
        private T result;
        private List<IoError> errors;

        public virtual bool Success => success;
        public virtual T Result => result;
        public virtual List<IoError> Errors => errors;

        protected RemoveResult()
        {
        }

        public static RemoveResult<T> Create(bool success, T result, IEnumerable<IoError> errors = null)
        {
            return new RemoveResult<T>
            {
                success = success,
                result = result,
                errors = errors != null ? errors.ToList() : new List<IoError>()
            };
        }
    }
}
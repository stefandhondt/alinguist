﻿using System;

namespace ALinguist.Shared
{
    [Serializable]
    public class StringFilter
    {
        public string Filter { get; set; }
        public bool IgnoreCase { get; set; }
        public StringCompareTypes CompareStrategy { get; set; }

        public override string ToString()
        {
            return $"{nameof(Filter)}: {Filter}, {nameof(IgnoreCase)}: {IgnoreCase}, {nameof(CompareStrategy)}: {CompareStrategy}";
        }
    }
}
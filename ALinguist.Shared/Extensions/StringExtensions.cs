﻿using System;
using System.Globalization;

namespace ALinguist.Shared.Extensions
{
    public static class StringExtensions
    {
        public static CultureInfo AsCulture(this string input)
        {
            try
            {
                return CultureInfo.GetCultureInfo(input);
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
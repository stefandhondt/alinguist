﻿using System;

namespace ALinguist.Shared
{
    [Serializable]
    public enum StringCompareTypes
    {
        StartsWith,
        EndsWith,
        Contains,
        DoesNotContain,
        ExactMatch
    }
}
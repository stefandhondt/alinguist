﻿using System.Windows;
using Alinguist.Client.ViewModels;
using Caliburn.Micro;

namespace Alinguist.Client
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<OverviewViewModel>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Alinguist.Client.ViewModels;

namespace Alinguist.Client.Views
{
    /// <summary>
    /// Interaction logic for OverviewView.xaml
    /// </summary>
    public partial class OverviewView : UserControl
    {
        public OverviewView()
        {
            InitializeComponent();
        }

        private void Translations_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(!(sender is ListView lv))
                return;

            if(!(lv.DataContext is OverviewViewModel vm))
                return;

            if(e.AddedItems != null)
                vm.SelectedTranslations.AddRange(e.AddedItems.Cast<TranslationViewModel>());

            if(e.RemovedItems != null)
                vm.SelectedTranslations.RemoveRange(e.RemovedItems.Cast<TranslationViewModel>());
        }
    }
}

﻿using Caliburn.Micro;

namespace Alinguist.Client.ViewModels
{
    public class FilterViewModel<T> : Screen
    {
        private int numberOfResults;
        private int pageSize;
        private int pages;
        private int page;

        public int Page
        {
            get => page;
            set
            {
                if(value == page) return;
                page = value;
                NotifyOfPropertyChange();
            }
        }

        public int Pages
        {
            get => pages;
            set
            {
                if(value == pages) return;
                pages = value;
                NotifyOfPropertyChange();
            }
        }

        public int PageSize
        {
            get => pageSize;
            set
            {
                if(value == pageSize) return;
                pageSize = value;
                NotifyOfPropertyChange();
            }
        }

        public int NumberOfResults
        {
            get => numberOfResults;
            set
            {
                if(value == numberOfResults) return;
                numberOfResults = value;
                NotifyOfPropertyChange();
            }
        }

        public IObservableCollection<T> Results { get; set; }

        public FilterViewModel()
        {
            Results = new BindableCollection<T>();
        }
    }
}
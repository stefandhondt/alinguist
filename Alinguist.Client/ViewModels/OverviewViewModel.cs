﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Alinguist.Algorithms.StringMatching;
using Alinguist.IO;
using Alinguist.IO.Model;
using Caliburn.Micro;

namespace Alinguist.Client.ViewModels
{
    public class OverviewViewModel : Screen
    {
        private SqlServerResourceReader resourceReader = SqlServerResourceReader.Create();

        public TranslationFilterViewModel Filter { get; set; }
        public BindableCollection<TranslationViewModel> Translations { get; set; }
        public BindableCollection<MatchedTranslationViewModel> MatchingTranslations { get; set; }
        public BindableCollection<TranslationViewModel> SelectedTranslations { get; set; }

        public OverviewViewModel()
        {
            SelectedTranslations = new BindableCollection<TranslationViewModel>();
            SelectedTranslations.CollectionChanged += SelectedTranslations_CollectionChanged;
            Filter = new TranslationFilterViewModel();
            Filter.PropertyChanged += delegate(object sender, PropertyChangedEventArgs args) { FilterTranslations(); };
            var translations = resourceReader.Read();

            var translationVms = translations.Result.Select(t => new TranslationViewModel
            {
                ResourceKey = t.Key.Id,
                Culture = t.Culture,
                Group = t.Group,
                Name = t.Name,
                Value = t.Value
            }).ToList();
            Translations = new BindableCollection<TranslationViewModel>(translationVms);
            MatchingTranslations = new BindableCollection<MatchedTranslationViewModel>();
            Filter.NotifyOfPropertyChange();
            PropertyChanged += OnPropertyChanged;
        }

        private void SelectedTranslations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => SelectedTranslations);
            NotifyOfPropertyChange(() => CanEdit);
            NotifyOfPropertyChange(() => CanRemove);
        }

        private void OnPropertyChanged(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            switch(propertyChangedEventArgs.PropertyName)
            {
                case nameof(SelectedTranslations):
                    MatchingTranslations.Clear();

                    if((SelectedTranslations?.Count() ?? 0) == 1)
                        MatchingTranslations.AddRange(FindMatchingTranslations(new Levenshtein(), SelectedTranslations.Single(), Translations).OrderBy(mt => mt.MatchResult));
                    break;
            }
        }

        private IEnumerable<MatchedTranslationViewModel> FindMatchingTranslations(IStringMatcher matcher, TranslationViewModel sourceTranslation, IEnumerable<TranslationViewModel> translations)
        {
            var result = new List<MatchedTranslationViewModel>();

            if(sourceTranslation == null
                || translations == null
                || !translations.Any())
                return result;

            result.AddRange(from tr in translations.Where(t => t != sourceTranslation)
                let match = matcher.Compute(sourceTranslation.Value, tr.Value)
                select new MatchedTranslationViewModel
                {
                    Translation = tr,
                    MatchResult = match
                });

            return result;
        }

        public void FilterTranslations()
        {
            if(Filter == null)
                return;

            var result = new List<TranslationViewModel>(Translations).AsQueryable();

            if(!string.IsNullOrWhiteSpace(Filter.Culture))
                result = result.Where(t => t.Culture.Contains(Filter.Culture));

            if(!string.IsNullOrWhiteSpace(Filter.Group))
                result = result.Where(t => t.Group.Contains(Filter.Group));

            if(!string.IsNullOrWhiteSpace(Filter.Name))
                result = result.Where(t => t.Name.Contains(Filter.Name));

            Filter.Results.Clear();
            Filter.Results.AddRange(result.ToList());
        }

        #region Commands

        public bool CanAdd => true;

        public void Add()
        {
            resourceReader.Write(ResourceData.Create("nl-BE", "abc", Guid.NewGuid().ToString().Substring(0, 8), Guid.NewGuid().ToString().Substring(0, 12)));
        }

        public bool CanEdit => SelectedTranslations != null && SelectedTranslations.Any();

        public void Edit()
        {
            
        }

        public bool CanRemove => SelectedTranslations != null && SelectedTranslations.Any();

        public void Remove()
        {
            
        }

        #endregion Commands
    }
}
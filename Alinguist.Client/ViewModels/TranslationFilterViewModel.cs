﻿namespace Alinguist.Client.ViewModels
{
    public class TranslationFilterViewModel : FilterViewModel<TranslationViewModel>
    {
        private string group;
        private string name;
        private string culture;

        public string Group
        {
            get => group;
            set
            {
                if(value == group) return;
                group = value;
                NotifyOfPropertyChange();
            }
        }

        public string Name
        {
            get => name;
            set
            {
                if(value == name) return;
                name = value;
                NotifyOfPropertyChange();
            }
        }

        public string Culture
        {
            get => culture;
            set
            {
                if(value == culture) return;
                culture = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
﻿using Caliburn.Micro;

namespace Alinguist.Client.ViewModels
{
    public class MatchedTranslationViewModel : Screen
    {
        private int? matchResult;
        private TranslationViewModel translation;

        public TranslationViewModel Translation
        {
            get => translation;
            set
            {
                if(Equals(value, translation)) return;
                translation = value;
                NotifyOfPropertyChange();
            }
        }

        public int? MatchResult
        {
            get => matchResult;
            set
            {
                if(value == matchResult) return;
                matchResult = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
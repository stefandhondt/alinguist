﻿using System.Globalization;
using Caliburn.Micro;

namespace Alinguist.Client.ViewModels
{
    public class TranslationViewModel : Screen
    {
        private int resourceKey;
        private string culture;
        private string value;
        private string name;
        private string group;

        public int ResourceKey
        {
            get => resourceKey;
            set
            {
                if(Equals(value, resourceKey)) return;
                resourceKey = value;
                NotifyOfPropertyChange();
            }
        }

        public string Culture
        {
            get => culture;
            set
            {
                if(Equals(value, culture)) return;
                culture = value;
                NotifyOfPropertyChange();
            }
        }

        public string Group
        {
            get => group;
            set
            {
                if(value == group) return;
                group = value;
                NotifyOfPropertyChange();
            }
        }

        public string Name
        {
            get => name;
            set
            {
                if(value == name) return;
                name = value;
                NotifyOfPropertyChange();
            }
        }

        public string Value
        {
            get => value;
            set
            {
                if(value == this.value) return;
                this.value = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
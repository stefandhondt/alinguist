﻿using System;

namespace Alinguist.Exceptions
{
    public class BusinessException : Exception
    {
        protected readonly int exceptionCode;

        public int ExceptionCode => exceptionCode;

        public BusinessException(int exceptionCode)
        {
            this.exceptionCode = exceptionCode;
        }
    }
}
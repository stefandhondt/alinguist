﻿using System;

namespace Alinguist.Exceptions.Resource
{
    [Serializable]
    public class NameEmptyException : BusinessException
    {
        public NameEmptyException()
            : base(ExceptionCodes.Resource.NameEmpty)
        {
        }
    }
}
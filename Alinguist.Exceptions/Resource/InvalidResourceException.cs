﻿namespace Alinguist.Exceptions.Resource
{
    public class InvalidResourceException : BusinessException
    {
        public InvalidResourceException()
            : base (ExceptionCodes.Resource.InvalidResource)
        {
        }
    }
}
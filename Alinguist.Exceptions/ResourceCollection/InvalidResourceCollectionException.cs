﻿using System;

namespace Alinguist.Exceptions.ResourceCollection
{
    [Serializable]
    public class InvalidResourceCollectionException : BusinessException
    {
        public InvalidResourceCollectionException()
            : base(ExceptionCodes.ResourceCollection.InvalidResourceCollection)
        {
        }
    }
}
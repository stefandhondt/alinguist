﻿using System;

namespace Alinguist.Exceptions.ResourceCollection
{
    [Serializable]
    public class NameEmptyException : BusinessException
    {
        public NameEmptyException()
            : base(ExceptionCodes.ResourceCollection.NameEmpty)
        {
        }
    }
}
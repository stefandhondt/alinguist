﻿namespace Alinguist.Exceptions
{
    public sealed class ExceptionCodes
    {
        public static class Culture
        {
            public static int InvalidCulture = 1001;
            public static int CultureEmpty = 1002;
        }

        public static class ResourceCollection
        {
            public static int InvalidResourceCollection = 2001;
            public static int NameEmpty = 2002;
            public static int DuplicateResourceName = 2003;
        }

        public static class Resource
        {
            public static int InvalidResource = 2101;
            public static int NameEmpty = 2102;
        }
    }
}
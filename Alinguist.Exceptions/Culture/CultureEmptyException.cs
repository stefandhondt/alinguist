﻿namespace Alinguist.Exceptions.Culture
{
    public class CultureEmptyException : BusinessException
    {
        public CultureEmptyException()
            : base(ExceptionCodes.Culture.CultureEmpty)
        {

        }
    }
}

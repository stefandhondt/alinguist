﻿namespace Alinguist.Exceptions.Culture
{
    public class InvalidCultureException : BusinessException
    {
        private readonly string culture;

        public string Culture => culture;

        public InvalidCultureException(string culture = null)
            : base (ExceptionCodes.Culture.InvalidCulture)
        {
            this.culture = culture;
        }
    }
}
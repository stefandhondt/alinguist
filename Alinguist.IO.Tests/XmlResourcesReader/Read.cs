﻿using System.Collections.Generic;
using System.Xml.Linq;
using Alinguist.IO.Parsers;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Alinguist.IO.Tests.XmlResourcesReader
{
    [TestClass]
    public class Read
    {
        private IO.XmlResourcesReader fakeReader;

        [TestInitialize]
        public void Init()
        {
            fakeReader = A.Fake<IO.XmlResourcesReader>();
            A.CallTo(() => fakeReader.Read()).CallsBaseMethod();
            A.CallTo(() => fakeReader.Validators).Returns(new List<Validation.IValidator>());
        }

        [TestMethod]
        public void WhenCalled_ValidateShouldBeCalled()
        {
            //Act
            fakeReader.Read();

            //Assert
            A.CallTo(() => fakeReader.Validate()).MustHaveHappened();
        }

        [TestMethod]
        public void WhenValidateReturnsErrors_ResultShouldBeUnsuccessfulAndContainValidationErrors()
        {
            //Arrange
            var errors = new List<IO.IoError>
            {
                A.Fake<IO.IoError>(),
                A.Fake<IO.IoError>(),
                A.Fake<IO.IoError>()
            };
            A.CallTo(() => fakeReader.Validate()).Returns(errors);

            //Act
            var result = fakeReader.Read();

            //Assert
            result.Success.ShouldBeFalse();
            result.Errors.ShouldBe(errors);
        }

        [TestMethod]
        public void WhenValidFileIsProvided_FileShouldBeProcessed()
        {
            //Arrange
            var name = "Test-Valid";
            var culture = "en";
            A.CallTo(() => fakeReader.Validate()).Returns(null);
            A.CallTo(() => fakeReader.Parse(A<XDocument>.Ignored)).CallsBaseMethod();
            var fakeFilenameParser = A.Fake<IFilenameParser<ResourcesFilenameParserResult>>();
            var filenameParserResult = A.Fake<ResourcesFilenameParserResult>();
            A.CallTo(() => filenameParserResult.Name).Returns(name);
            A.CallTo(() => filenameParserResult.Culture).Returns(culture);
            A.CallTo(() => fakeFilenameParser.Parse()).Returns(filenameParserResult);
            A.CallTo(() => fakeReader.FilenameParser).Returns(fakeFilenameParser);
            A.CallTo(() => fakeReader.Filename).Returns(@"Resources\Test-Valid_en.resources");

            //Act
            var readResult = fakeReader.Read();

            //Assert
            readResult.Success.ShouldBeTrue();
            readResult.Result.Name.ShouldBe(name);
            readResult.Result.Culture.Name.ShouldBe(culture);
            readResult.Result.Resources.Count.ShouldBe(3);
        }
    }
}

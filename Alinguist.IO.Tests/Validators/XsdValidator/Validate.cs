﻿using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Alinguist.IO.Validation;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Alinguist.IO.Tests.Validators.XsdValidator
{
    [TestClass]
    public class Validate
    {
        private Validation.XsdValidator fakeValidator;
        private string validXml = @"Resources\Test-Valid_en.resources";
        private const string invalidStructureXml = @"Resources\Test-InvalidXmlStructure_en.resources";
        private const string invalidStructure2Xml = @"Resources\Test-InvalidXmlStructure2_en.resources";
        private const string schemaFilename = @"Validation\XSD\Resources.xsd";

        [TestInitialize]
        public void Init()
        {
            fakeValidator = A.Fake<Validation.XsdValidator>();
            A.CallTo(() => fakeValidator.Validate()).CallsBaseMethod();

            var xml = XDocument.Load(validXml);
            A.CallTo(() => fakeValidator.Xml).Returns(xml);

            var xmlSchemas = new XmlSchemaSet();
            xmlSchemas.Add(xml.Root.GetNamespaceOfPrefix("i").NamespaceName, schemaFilename);
            A.CallTo(() => fakeValidator.XmlSchemas).Returns(xmlSchemas);
        }

        [TestMethod]
        public void WhenValidXmlAndXsdsAreProvided_ValidationShouldSucceed()
        {
            //Act
            var result = fakeValidator.Validate();

            //Assert
            result.Valid.ShouldBeTrue();
            result.Errors.ShouldBeEmpty();
        }

        [TestMethod]
        public void WhenNoXmlIsProvided_ValidationShouldFail()
        {
            //Arrange
            A.CallTo(() => fakeValidator.Xml).Returns(null);

            //Act
            var result = fakeValidator.Validate();

            //Assert
            result.Valid.ShouldBeFalse();
            result.Errors.ShouldContain(e => e.ErrorCode == ValidationCodes.Xsd.NoXmlProvided);
        }

        [TestMethod]
        public void WhenNoXmlSchemasAreProvided_ValidationShouldFail()
        {
            //Arrange
            A.CallTo(() => fakeValidator.XmlSchemas).Returns(null);

            //Act
            var result = fakeValidator.Validate();

            //Assert
            result.Valid.ShouldBeFalse();
            result.Errors.ShouldContain(e => e.ErrorCode == ValidationCodes.Xsd.NoXmlSchemaProvided);
        }

        [TestMethod]
        public void WhenInvalidXmlIsProvided_ValidationShouldFail()
        {
            //Arrange
            var xml = XDocument.Load(invalidStructure2Xml);
            A.CallTo(() => fakeValidator.Xml).Returns(xml);

            //Act
            var result = fakeValidator.Validate();

            //Assert
            result.Valid.ShouldBeFalse();
            result.Errors.ShouldContain(e => e.ErrorCode == ValidationCodes.Xsd.XmlSchemaValidationFailed);
        }
    }
}